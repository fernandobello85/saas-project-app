$(document).ready(function(){
  $(function() {
    $('.datepicker').datepicker({
      title: 'Expected completion date',
      format: 'yyyy-mm-dd',
      language: 'en',
      orientation: 'bottom',
      autoclose: true
    })
  });
});