class Artifact < ApplicationRecord
  belongs_to :project
  validates :name, presence: true, uniqueness: true
  validate :acceptable_upload
  has_one_attached :upload

  MAX_FILESIZE = 10.megabytes

  def acceptable_upload
    return unless upload.attached?

    errors.add(:upload, "File size must be less than #{self.class::MAX_FILESIZE}") if upload.byte_size > 5.megabytes

    # acceptable_types = ['image/jpeg', 'image/png']
    # unless acceptable_types.include?(avatar.content_type)
    #   errors.add(:avatar, 'must be a JPEG or PNG')
    # end
  end
end
