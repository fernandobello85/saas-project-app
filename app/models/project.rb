class Project < ApplicationRecord
  belongs_to :tenant
  validates_uniqueness_of :title
  validate :free_plan_can_only_have_one_project
  has_many :artifacts, dependent: :destroy
  has_many :user_projects, foreign_key: :project_id, dependent: :destroy
  has_many :user, through: :user_projects

  def free_plan_can_only_have_one_project
    return unless new_record? && tenant.projects.any? && tenant.plan == 'free'

    errors.add(:base, 'Free plans cannot have more than one project')
  end

  def self.by_user_plan_and_tenant(tenant_id, user)
    tenant = Tenant.find(tenant_id)
    if tenant.plan == 'premium'
      if user.is_admin?
        tenant.projects
      else
        user.projects.where(tenant_id: tenant.id)
      end
    else
      if user.is_admin?
        tenant.projects.order(:id).limit(1)
      else
        user.projects.where(tenant_id: tenant.id).order(:id).limit(1)
      end
    end
  end
end
