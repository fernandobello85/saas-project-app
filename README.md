# README
Project Management App is a SAAS app developed in Ruby from the Complete Ruby on Rails Developer cours.

App Requirements :
 * A user can sign up the app and automatically enrolled as Admin of a new tenant. 
 * Sign up requires email confirmation.
 * A tenant administrator can add new members that recive an email invitation to join the tenant.
 * Administrator and members of a tenant can create Projects for this tenant.
 * Projects created in a tenant are accessibles only for the admin and members of the tenant.
 * A project has artifacts with attachments.
 * A Free tenant allows create only one project. 
 * A Premium tenant allows create unlimited projects.
 * Premium tenant must be paid using credit card and the price is $10 USD

Technical environment :
Ruby version 2.6.3
Rails version 6.0.3.2
Twitter bootstrap Rails 4.0.0
Stripe
AWS S3 


[Heroku app](https://manage-project-app.herokuapp.com/)
